# How to contribute #

You're free to contribute, but here are some rules :

## Commit names ##

Your commit names should be one of the following :  

- Tuturuuu : Normal commit
- Tuturoopsy : Commit that fixes errors YOU made
- Tuturuwu : Commit that fixes errors SOMEONE ELSE made
- TUTURUUUUU(...)UU : major advancement
- Tuturu. : Refacto / Clean up (can be TUTURUUUUUU if it's big enough)

## Other rules ##

Please use japanese / weeb smileys in the Mayuri's messages