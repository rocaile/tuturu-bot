﻿using System.Threading.Tasks;
using Discord;
using Discord.Interactions;
using RunMode = Discord.Interactions.RunMode;

namespace Bot.Modules;

[ModuleDescription("🎉 Fun / Meme commands :")]
public class GarbageModule : Module<SocketInteractionContext>
{
    private const string MerguezEmote = "<:merguez:825485788946628609>";
    private const string BrouetteEmote = "<:brouette:825482835103580210>";
    private const string DesoleEmote = "<:desole:825501481385459722>";
    private const string DanceEmote = "<:dance:831272010314940437>";
    private const string UberEatsEmote = "<:ubereats:825486500263100416>";
    private const string MortarEmote = "<:mortar:825504401501978694>";
    private const string ScooterEmote = "<:scootervole:825505123697557512>";
    private const string BallsEmote = "<:balls:833798434070331412>";
    private const string DaftEmote = "<:daft:833794395546583070>";
    private const string PunkEmote = "<:punk:833794413238419496>";
    private const string PeeEmote = "<:pee:833798449920081921>";

    private MusicPlayerService _musicPlayerService;
    public GarbageModule(MusicPlayerService musicPlayerService)
    {
        _musicPlayerService = musicPlayerService;
    }
        
    [SlashCommand("nounours", "Nounours !")]
    public async Task Nounours()
    {
        await RespondAsync("https://www.youtube.com/watch?v=_N4XunFaSnU");
    }

    [SlashCommand("desole", DesoleEmote + " Nous sommes désolay", runMode: RunMode.Async)]
    public async Task Desole()
    {
        await RespondAsync(DesoleEmote);
        await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=HkEqSIFtaAA", Context);
    }

    [SlashCommand("four", UberEatsEmote + " Go grab your order !", runMode: RunMode.Async)]
    public async Task Four()
    {
        await RespondAsync($"{UberEatsEmote} https://www.ubereats.com/fr");
        await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=czTksCF6X8Y", Context);
    }
    
    [SlashCommand("woyoyo", "Woyoyo, woyoyoyo", runMode: RunMode.Async)]
    public async Task Woyoyo()
    {
        await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=-VTN2oT321s", Context);
    }

    [SlashCommand("mortar", MortarEmote + " Because the scooter mortar l'avait volé", runMode: RunMode.Async)]
    public async Task Mortar()
    {
        if (Context.User.Id == 440859766840098816)
        {
            await RespondAsync($"{MortarEmote} Zou Zou Zou Zoubida ! {ScooterEmote}");
            await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=RFQYRq2MB5U", Context);
        }
        else
        {
            await RespondAsync("Y'a pas moyen djadja");
            await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=xc9fCzoszDs", Context);
        }
    }
        
    [SlashCommand("brouette", BrouetteEmote + "Je ne suis pas ~~un héros~~ une brouette", runMode: RunMode.Async)]
    public async Task Brouette(string @long = null)
    {
        await RespondAsync("Arrêtez de m'appeller brouette ... T~T");
        await Context.Interaction.GetOriginalResponseAsync().Result.AddReactionAsync(Emote.Parse(BrouetteEmote));
        string url;
        if (@long == "full" || @long == "long")
            url = "https://www.youtube.com/watch?v=Vw_9CrHEwYU";
        else
            url = "https://youtu.be/I2isnUEbdgw";
        await _musicPlayerService.PlayMusic(url, Context);
    }
        
    [SlashCommand("merguez", MerguezEmote + "Oui allez on va bien s'amuser !", runMode: RunMode.Async)]
    public async Task MerguezParty()
    {
        await RespondAsync(MerguezEmote);
        await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=P51eTG1dp9I", Context);
    }
        
    [SlashCommand("noel", "Suite logique de la merguez party", runMode: RunMode.Async)]
    public async Task PereNoel()
    {
        await RespondAsync("Ho ho hooo");
        await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=fErXRHPIpgA", Context);
    }
        
    [SlashCommand("dance", DanceEmote + "YOU ARE A DANCING QUEEEEEEN", runMode: RunMode.Async)]
    public async Task DancingQueen()
    {
        await RespondAsync("https://media.giphy.com/media/l3vR5P5d9u7TWajqU/giphy.gif");
        await Context.Interaction.GetOriginalResponseAsync().Result.AddReactionAsync(Emote.Parse(DanceEmote));
        await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=XqBWvmhS-AY", Context);
    }

    [SlashCommand("tremblement", "Pi pila pi pi pou c'est comme pi pou un tremblement de terre pi pilou", runMode: RunMode.Async)]
    public async Task EarthquakeOfheLove()
    {
        await RespondAsync("https://tenor.com/view/earthquake-shaking-cartoon-animated-shaky-bed-gif-16616197");
        await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=4GHCkmTBEi4", Context);
    }
        
    [SlashCommand("pee", "Mais où est il stocké ?", runMode: RunMode.Async)]
    public async Task PeeBalls()
    {
        await RespondAsync(DaftEmote + BallsEmote + "⬅" + PeeEmote + PunkEmote);
        await _musicPlayerService.PlayMusic("https://www.youtube.com/watch?v=pKQp61e94VE", Context);
    }

    [SlashCommand("anniversaire", "Because everyday is Grimmby's birthday", runMode: RunMode.Async)]
    public async Task Anniversaire()
    {
        await RespondAsync("JOYEUX ANNIVERSAIRE CAMILLE !!");
        const string url = "https://www.youtube.com/watch?v=zbuShFEOQtk";
        await _musicPlayerService.PlayMusic(url, Context);
    }

    [SlashCommand("hsotl", "Our secret project's ETA")]
    public async Task Hsotl()
    {
        await RespondAsync("WIP : promis Rocail travaille dessus à plein temps (au moins 10 minutes par an). De toute façon à la fin le héros meurt parce qu'il s'est mis toutes les filles à dos.");
    }
        
    [RequireUserPermission(GuildPermission.ManageChannels, Group = "Permission")]
    [SlashCommand("talk", "let her talk !")]
    public async Task Talk(string message = null)
    {
        await ReplyAsync("Processing ...");
        await Context.Interaction.DeleteOriginalResponseAsync();
        await Context.Channel.SendMessageAsync(message);
    }
}