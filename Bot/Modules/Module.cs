﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Interactions;

namespace Bot.Modules
{
    public class Module<T> : InteractionModuleBase<T> where T : SocketInteractionContext
    {
        protected async Task SendSimpleEmbedMessage(string message, IMessageChannel channel)
        {
            var eb = MessageHelper.SimpleEmbed(message);
            await channel.SendMessageAsync("", false, eb.Build());
        }

        protected async Task RespondSimpleEmbed(string message, bool ephemeral = false)
        {
            await RespondAsync("", [MessageHelper.SimpleEmbed(message).Build()], ephemeral: ephemeral);
        }

        protected async Task FollowupSimpleEmbed(string message, bool ephemeral = false)
        {
            await FollowupAsync("", [MessageHelper.SimpleEmbed(message).Build()], ephemeral: ephemeral);
        }

        protected override Task RespondAsync(string text = null, Embed[] embeds = null, bool isTTS = false, bool ephemeral = false,
            AllowedMentions allowedMentions = null, RequestOptions options = null, MessageComponent components = null,
            Embed embed = null)
        {
            if (new Random().Next(1, 1000) == 50)
            {
                return base.RespondAsync(null, null, isTTS, false, allowedMentions, options, components,
                    MessageHelper.InitCreepyEmbedMessage().Build());
            }
            return base.RespondAsync(text, embeds, isTTS, ephemeral, allowedMentions, options, components, embed);
        }

        // The ephemeral of the first call to FollowupAsync is ignored
        protected override Task<IUserMessage> FollowupAsync(string text = null, Embed[] embeds = null, bool isTTS = false, bool ephemeral = false,
            AllowedMentions allowedMentions = null, RequestOptions options = null, MessageComponent components = null,
            Embed embed = null)
        {
            if (new Random().Next(1, 1000) == 50)
            {
                Context.Interaction.Channel.SendMessageAsync(embed: MessageHelper.InitCreepyEmbedMessage().Build());
                return base.FollowupAsync("...");
            }
            return base.FollowupAsync(text, embeds, isTTS, ephemeral, allowedMentions, options, components, embed);
        }
    }
}

public class ModuleDescription : Attribute
{
    public readonly string Description;
    public ModuleDescription(string description)
    {
        Description = description;
    }
}