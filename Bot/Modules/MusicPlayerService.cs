﻿using System;
using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;
using Bot.MusicServices;
using Discord;
using Discord.Interactions;

namespace Bot.Modules;

public class MusicPlayerService
{
    private readonly MusicPlayer _musicPlayer;
    private readonly string _tuturuHelloPath;

    public MusicPlayerService(MusicPlayer musicPlayer)
    {
        _musicPlayer = musicPlayer;
            
        var homePath = Environment.OSVersion.Platform == PlatformID.Unix ||
                       Environment.OSVersion.Platform == PlatformID.MacOSX
            ? Environment.GetEnvironmentVariable("HOME")
            : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
        if (homePath != null)
        {
            const string tuturuFileName = "tuturu-hello.mp3";
            _tuturuHelloPath = Path.Combine(homePath, tuturuFileName);
        }
        else
        {
            _tuturuHelloPath = "";
        }
    }

    private void VerifyUrl(string url)
    {
        ArgumentNullException.ThrowIfNull(url, "No url specified ! Noob haha xptdr x'3");

        if (!(Uri.TryCreate(url, UriKind.Absolute, out var uri)
              && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps)))
        {
            throw new ArgumentException("This is not a valid url ! Lol u so noob even my granma is a lesser noob than u hahaha");
        }
    }

    private IVoiceChannel GetUserVoiceChannel(SocketInteractionContext context)
    {
        var channel = (context.User as IGuildUser)?.VoiceChannel;
        ArgumentNullException.ThrowIfNull(channel, "You must be in a voice channel baka !");

        return channel;
    }

    private string PlayMusicResponse(int count, SocketInteractionContext context)
    {
        string name;
        var author = context.User as IGuildUser;

        if (author?.Nickname == null || author.Nickname == "")
        {
            name = context.User.Username;
        }
        else
        {
            name = author.Nickname;
        }
        return count > 0 ? 
            $"Thanks {name} ! Your music{(count > 1 ? "s" : "")} has been added to the queue (≧∇≦)/" :
            $"Sorry {name}, Mayushi searched a lot but haven't found your video ... Gomenasai (ㄒoㄒ)";
    }    
    
    public async Task<(int, string)> PlayMusic(string musicUrl, SocketInteractionContext context)
    {
        var count = 0;
        IVoiceChannel channel;

        try {
            VerifyUrl(musicUrl);
            channel = GetUserVoiceChannel(context);
        } catch (ArgumentException ex) {
            return (0, "You must be in a voice channel baka !");
        }

        IVoiceChannel connectedChannel = context.Guild.CurrentUser.VoiceChannel;

        Task connection = null;

        if (connectedChannel == null || connectedChannel.Id != channel.Id || context.Guild.AudioClient == null ||
            _musicPlayer.StreamSender.GetClient() == null) // tema la taille de la ligne
        {
            if (connectedChannel != null)
            {
                await _musicPlayer.Stop();
                await connectedChannel.DisconnectAsync();
            }
            connection = ConnectToVoiceChannel(channel);
        }
            
        count = await _musicPlayer.AddMusic(musicUrl);

        if (connection != null)
        {
            await connection;
        }

        _musicPlayer.Play();

        return (count, PlayMusicResponse(count, context));
    }

    public async Task ConnectToVoiceChannel(IAudioChannel channel)
    {
        _musicPlayer.StreamSender.SetClient(await channel.ConnectAsync());
        await _musicPlayer.SendSingleLocaleMusic(_tuturuHelloPath);
    }

    public async Task Disconnect(SocketInteractionContext context)
    {
        await _musicPlayer.Stop();
        await context.Guild.CurrentUser.VoiceChannel.DisconnectAsync();
    }

    public async Task Stop()
    {
        await _musicPlayer.Stop();
    }

    public async Task Skip()
    {
        await _musicPlayer.Skip();
    }

    public bool Loop()
    {
        return _musicPlayer.Loop();
    }
}