﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.Interactions;
using RunMode = Discord.Interactions.RunMode;

namespace Bot.Modules;

public class HelpModule : Module<SocketInteractionContext>
{
    private readonly InteractionService _interactionService;

    public HelpModule(InteractionService commands)
    {
        _interactionService = commands;
    }

    [SlashCommand("help", "Let's see what Mayushii can do !", runMode: RunMode.Async)]
    public async Task Help()
    {
        var eb = MessageHelper.InitEmbedMessage();
        eb.WithTitle("Tuturu ! Here are the commands to bring some Mayushii into your heart !");

        var modules = _interactionService.Modules;

        foreach (var moduleInfo in modules)
        {
            var fieldTitle =
                (moduleInfo.Attributes.FirstOrDefault(atr => atr.GetType() == typeof(ModuleDescription)) as
                    ModuleDescription)?.Description;
            if (fieldTitle == null) continue;
            var fieldDescription = new StringBuilder();
            var commands = moduleInfo.SlashCommands.ToList();
            commands.Sort((x, y) => string.CompareOrdinal(x.Name, y.Name));

            foreach (var command in commands)
            {
                if (command.Description == null) continue;

                var args = command.Parameters.Aggregate("",
                    (sum, p) => p.IsRequired ? $"**<{p.Name}>** " : $"**[{p.Name}]**$ ");
                args = args.TrimEnd();
                fieldDescription.AppendLine($"**/{command.Name}** {args} : {command.Description}");
            }

            if (fieldDescription.Length == 0) continue;

            eb.AddField(fieldTitle, fieldDescription);
        }

        await DeferAsync(ephemeral:true);
        await FollowupAsync("", new[]{ eb.Build() }, ephemeral: false);
    }
}