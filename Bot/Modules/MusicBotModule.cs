﻿using System.Threading.Tasks;
using Discord;
using Discord.Interactions;
using RunMode = Discord.Interactions.RunMode;

namespace Bot.Modules;

[ModuleDescription("🎵 Music bot commands :")]
public class MusicBotModule : Module<SocketInteractionContext>
{
    private MusicPlayerService _musicPlayerService;
    public MusicBotModule(MusicPlayerService musicPlayerService)
    {
        _musicPlayerService = musicPlayerService;
    }

    // The command's Run Mode MUST be set to RunMode.Async, otherwise, being connected to a voice channel will block the gateway thread.
    [SlashCommand("play", "Play a music from YouTube", runMode: RunMode.Async)]
    public async Task PlayMusicCommand(string url)
    {
        await DeferAsync(ephemeral: true);
        await Task.Run(async () =>
        {
            var count = await _musicPlayerService.PlayMusic(url, Context);
            await FollowupSimpleEmbed(count.Item2, ephemeral: true);
        });
    }

    [SlashCommand("skip", "Skip the current music", runMode: RunMode.Async)]
    public async Task SkipMusic()
    {
        await _musicPlayerService.Skip();
        await RespondAsync("Skipped !");
    }

    [SlashCommand("loop", "Enable/disable looping musics !", runMode: RunMode.Async)]
    public async Task LoopMusic()
    {
        if (_musicPlayerService.Loop())
        {
            await RespondAsync("Looping enabled ! Let's have FUN ! (╯°▽°)╯ ┻━┻");
        }
        else
        {
            await RespondAsync("Looping disabled ! Let's chill from now ┬─┬ ノ( ゜-゜ノ)");
        }
    }

    [SlashCommand("stop", "Stop the current music and delete all queued musics", runMode: RunMode.Async)]
    public async Task StopMusic()
    {
        await _musicPlayerService.Stop();
        await RespondSimpleEmbed("Stopped ╥﹏╥");
    }

    [SlashCommand("connect", "Say hi to Mayushii !", runMode: RunMode.Async)]
    public async Task Connect()
    {
        var channel = (Context.User as IGuildUser)?.VoiceChannel;
        if (channel == null)
        {
            await RespondSimpleEmbed("User must be in a voice channel");
            return;
        }

        if (channel.Id == Context.Guild.CurrentUser.VoiceChannel?.Id)
        {
            await RespondSimpleEmbed("Mayushi is already there ＠＾▽＾＠");
            return;
        }

        var userVoiceChannel = (Context.User as IGuildUser)?.VoiceChannel;
        await _musicPlayerService.ConnectToVoiceChannel(userVoiceChannel);
    }

    [SlashCommand("disconnect", "Say goodbye to Mayushii ... (╥_╥)", runMode: RunMode.Async)]
    public async Task Disconnect()
    {
        if (Context.Guild.CurrentUser.VoiceChannel == null)
        {
            await RespondSimpleEmbed(
                "B-But ... Senpai ... I'm not even on a voice channel ... why do you do this to me ?");
            return;
        }

        await _musicPlayerService.Disconnect(Context);
        await RespondSimpleEmbed("Farewell ... ＿|￣|O");
    }
}