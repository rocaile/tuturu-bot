﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bot.MusicServices;

public class MusicPlayer(StreamSender streamSender)
{
    private int _count;
    public readonly StreamSender StreamSender = streamSender;
    private Queue<Music> _musics = new();
    private bool _isPlaying;
    private bool _isLooping;

    public async Task<int> AddMusic(string url)
    {
        int nbMusics;

        var playlist = new Playlist(url);

        _count = Math.Max(_count + 1, _musics.Count);
        var musics = await playlist.Download(_count);
        musics.ForEach(music => _musics.Enqueue(music));
        nbMusics = musics.Count;
            

        return nbMusics;
    }

    public async Task Play()
    {
        if (_isPlaying || StreamSender.GetClient() == null) return;

        _isPlaying = true;

        while (_musics.TryDequeue(out var nextMusic))
        {
            try
            {
                await StreamSender.SendYoutubeMusicAsync(nextMusic.FilePathDir);
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                // j'en ai marre que ça crash donc on log juste et je debug au cas par cas
                Console.Error.WriteLine(e);
            }

            if (!_isLooping) continue;
            nextMusic.Looped = true;
            _musics.Enqueue(nextMusic);
        }

        _isPlaying = false;
        _count = 0;
    }

    public async Task Skip()
    {
        if (StreamSender.GetClient() == null) return;
        await StreamSender.StopStreaming();
    }

    public bool Loop()
    {
        _isLooping = !_isLooping;
        if (!_isLooping)
        {
            _musics = _musics.Where(m => !m.Looped) as Queue<Music> ?? new Queue<Music>();
        }

        return _isLooping;
    }

    public async Task Stop()
    {
        if (StreamSender.GetClient() == null) return;
        _isLooping = false;
        _musics.Clear();
        await StreamSender.StopStreaming();
        _isPlaying = false;
        _count = 0;
    }

    public async Task SendSingleLocaleMusic(string path)
    {
        await StreamSender.SendLocalFileAsync(path);
    }
}