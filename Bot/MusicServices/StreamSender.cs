﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Discord.Audio;

namespace Bot.MusicServices;

public class StreamSender
{
    private CancellationTokenSource _stopMusic;
    private AudioOutStream _audioStream;
    private IAudioClient _client;
    private Task isPlaying = null;
        
    public IAudioClient GetClient()
    {
        Console.WriteLine($"client : {_client != null} | stream : {_audioStream != null}");
        return _client;
    }
        
    public void SetClient(IAudioClient client)
    {
        _client = client;
        _audioStream = _client?.CreatePCMStream(AudioApplication.Mixed);
    }

    public async Task SendLocalFileAsync(string path)
    {
        using var ffmpeg = CreateLocalStream(path);
        var stream = ffmpeg.StandardOutput.BaseStream;
        if (stream.CanRead && _audioStream.CanWrite)
        {
            isPlaying = stream.CopyToAsync(_audioStream, 3840);
            await isPlaying;
        }
    }

    public async Task SendYoutubeMusicAsync(string url)
    {
        using var ffmpeg = CreateYoutubeStream(url);
        _stopMusic = new CancellationTokenSource();
        var ct = _stopMusic.Token;
        await using var stream = ffmpeg.StandardOutput.BaseStream;
        
        try
        {
            if (isPlaying != null)
            {
                if (!isPlaying.IsCompleted)
                {
                    Console.WriteLine("En train de jouer, on attend ! Chut !");
                    await isPlaying;
                }
                isPlaying = null;
            }
            if (_audioStream.CanWrite)
            {
                isPlaying = stream.CopyToAsync(_audioStream,3840, ct);
                await isPlaying;
            }
        }
        catch (OperationCanceledException e)
        {
            Console.WriteLine("ANNULATION DU COUP PRECEDENT");
            Console.WriteLine(e);
        }
        catch (Exception e)
        {
            Console.WriteLine("Impossible de lire la musique : " + url);
            Console.WriteLine(e);
        }
        finally
        {
            await _audioStream.FlushAsync();
            _stopMusic.Dispose();
        }
    }

    public Task StopStreaming()
    {
        if (_stopMusic == null || _stopMusic.IsCancellationRequested || isPlaying == null || isPlaying.IsCompleted) return Task.CompletedTask;
        
        _stopMusic.Cancel();

        return Task.CompletedTask;
    }

    private static Process CreateLocalStream(string path)
    {
        var cmd = Process.Start(new ProcessStartInfo
        {
            FileName = "ffmpeg",
            Arguments = $"-hide_banner -loglevel panic -i \"{path}\" -ac 2 -f s16le -ar 48000 pipe:1",
            UseShellExecute = false,
            RedirectStandardOutput = true
        });
        return cmd;
    }

    private static Process CreateYoutubeStream(string filePath)
    {
        return CreateLocalStream(filePath);
    }
}