﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bot.MusicServices;

public class Playlist
{
    private static readonly string HomeDir = Environment.OSVersion.Platform == PlatformID.Unix ||
                                             Environment.OSVersion.Platform == PlatformID.MacOSX
        ? Environment.GetEnvironmentVariable("HOME")
        : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
    private readonly string _url;

    public Playlist(string url)
    {
        _url = url;
    }

    public Task<List<Music>> Download(int musicNumber)
    {
        DeleteLocalFile(musicNumber);
        return DownloadLocalFile(musicNumber);
    }

    private static void DeleteLocalFile(int musicNumber)
    {
        var dir = new DirectoryInfo(HomeDir);

        const int numberOfRetries = 3;
        const int delayOnRetry = 1000;

        for (var i = 0; i < numberOfRetries; i++)
        {
            try
            {
                foreach (var file in dir.EnumerateFiles($"{musicNumber}.*.mp3"))
                {
                    file.Delete();
                }

                break;
            }
            catch (IOException)
            {
                Thread.Sleep(delayOnRetry);
            }
        }
    }

    private async Task<List<Music>> DownloadLocalFile(int musicNumber)
    {
        var list = new List<Music>();
        const int numberOfRetries = 3;

        for (var i = 0; i < numberOfRetries; i++)
        {
            var outputPath = Path.Combine(HomeDir, $"{musicNumber}.%(playlist_index)s.mp3");
            var cmd = Process.Start(new ProcessStartInfo
            {
                FileName = "yt-dlp",
                Arguments = $"-x --audio-format mp3 {_url} -o {outputPath} --playlist-end 10 --audio-quality 0",
                UseShellExecute = false
            });

            if (cmd == null)
            {
                return [];
            }
                
            await cmd.WaitForExitAsync();

            var singleMusicPath = Path.Combine(HomeDir, $"{musicNumber}.NA.mp3");
                
            if (File.Exists(singleMusicPath))
            {
                list = [new Music(singleMusicPath)];
                break;
            }

            var dir = new DirectoryInfo(HomeDir);

            var playlist = dir.EnumerateFiles($"{musicNumber}.*.mp3").ToList();
            if (playlist.Count == 0) continue;
            list = playlist.Select(file => new Music(file.FullName)).ToList();
            list = list.OrderBy(o => int.Parse(o.FilePathDir.Split(".")[1])).ToList();
            break;
        }

        return list;
    }
}