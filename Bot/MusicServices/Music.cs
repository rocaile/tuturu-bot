﻿namespace Bot.MusicServices;

public class Music
{
    public bool Looped = false;
    public string FilePathDir { get; }
        
    public Music(string filePathDir)
    {
        FilePathDir = filePathDir;
    }
}