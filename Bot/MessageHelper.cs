using System;
using System.Threading.Tasks;
using Discord;
using Discord.Interactions;

namespace Bot;

public static class MessageHelper
{
    private static readonly string[] Messages =
    [
        "EXTERMINER ... EXTERMINER ..."
    ];

    public static EmbedBuilder SimpleEmbed(string message)
    {
        return InitEmbedMessage().WithDescription(message);
    }

    public static EmbedBuilder InitEmbedMessage()
    {
        var eb = new EmbedBuilder();
        eb.WithColor(Color.Blue);
        eb.WithThumbnailUrl(
            "https://i.pinimg.com/236x/b0/67/9d/b0679ddceb5ff8680604b0db157ed6ec--anime-music-gates.jpg");
        return eb;
    }

    public static EmbedBuilder InitCreepyEmbedMessage()
    {
        var eb = new EmbedBuilder();
        eb.WithColor(Color.Red);
        eb.WithThumbnailUrl(
            "https://www.cdiscount.com/pdt2/7/7/8/1/700x700/auc0801269064778/rw/t-800-terminator-tete-replique-officielle-23cm.jpg");
        eb.WithDescription(Messages[new Random().Next(0, Messages.Length)]);
        return eb;
    }
}