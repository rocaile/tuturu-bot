﻿using System;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Bot.Datasource;
using Bot.Modules;
using Bot.MusicServices;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver.Linq;

namespace Bot;

internal static class Program
{
    private static DiscordSocketClient _client;
    private static InteractionService _interactionService;
    private static bool _started;
        
    private static readonly ServiceProvider Services = new ServiceCollection()
        .AddSingleton<StreamSender>()
        .AddSingleton<MusicPlayer>()
        .AddSingleton<MusicPlayerService>()
        .AddSingleton<DiscordSocketClient>()
        .AddSingleton<HttpClient>()
        .BuildServiceProvider();

    private static void Main()
        => MainAsync().GetAwaiter().GetResult();

    private static async Task MainAsync()
    {
        var config = new DiscordSocketConfig
        {
            GatewayIntents = GatewayIntents.All
        };

        var client = new DiscordSocketClient(config);

        client.Log += LogAsync;
        client.MessageReceived += HandleMessageReceived;
        client.Ready += Ready;

        await client.LoginAsync(TokenType.Bot, Environment.GetEnvironmentVariable("DiscordToken"));

        _client = client;
        await _client.StartAsync();

        await Task.Delay(-1);
    }

    private static async Task HandleMessageReceived(SocketMessage messageParam)
    {
        if (messageParam is not SocketUserMessage message) return;
        if (message.Author.IsBot) return;
        
        var coquilletteEmote = Emote.Parse("<:coquillette:1176497010082336787>");
        if (new Random().Next(1, 1000) == 1)
        {
            await message.AddReactionAsync(coquilletteEmote);
            await UserExtensions.SendMessageAsync(_client.GetUser(200580270649376778), embed: MessageHelper.SimpleEmbed($"{message.Author.GlobalName} a été coquilleté dans {message.Channel.Name} !\n\nMessage d'origine:\n\"{message}\"").Build());
            await UserRepository.GetInstance().AddCoquillette(userId: message.Author.Id, message.Author.GlobalName);
        }

        if (message.Author is not SocketGuildUser author) return;
        if (author.Roles.Any(role => role.Id == 1250907975300808754))
        {
            await message.Channel.SendMessageAsync("La personne au dessus a été cancel, veuillez ne pas lui répondre");
        }
    }

    private static async Task Ready()
    {
        _interactionService = new InteractionService(_client);
        await _interactionService.AddModulesAsync(Assembly.GetEntryAssembly(), Services);
        await _interactionService.RegisterCommandsGloballyAsync();
        _interactionService.Log += LogAsync;

        _client.InteractionCreated += async interaction =>
        {
            var ctx = new SocketInteractionContext(_client, interaction);
            await _interactionService.ExecuteCommandAsync(ctx, Services);
        };

        if (_started) return;

        var eb = new EmbedBuilder();
        eb.WithColor(Color.Blue);
        eb.WithThumbnailUrl(
            "https://i.pinimg.com/236x/b0/67/9d/b0679ddceb5ff8680604b0db157ed6ec--anime-music-gates.jpg");
        eb.Description = "**🔥 Une nouvelle version de Mayushii vient d'arriver ! 🔥** ";
        foreach (var guild in _client.Guilds)
        {
            var channel = GetAnnouncementChannel(guild);
            if (channel != null)
            {
                await channel.SendMessageAsync(
                    "", false, eb.Build());
            }
        }

        _started = true;
    }

    private static IMessageChannel GetAnnouncementChannel(SocketGuild guild)
    {
        if (guild.Channels.FirstOrDefault(c => c.Name == "mayushii-updates") is IMessageChannel rootChannel)
        {
            return rootChannel;
        }

        foreach (var category in guild.CategoryChannels)
        {
            if (category.Channels.FirstOrDefault(c => c.Name == "mayushii-updates") is IMessageChannel subChannel)
            {
                return subChannel;
            }
        }

        return null;
    }

    private static Task LogAsync(LogMessage log)
    {
        Console.WriteLine(log.ToString());

        return Task.CompletedTask;
    }
}