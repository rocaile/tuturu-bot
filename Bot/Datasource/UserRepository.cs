using System;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Clusters;

namespace Bot.Datasource;

public class UserRepository
{
    private readonly string connectionString;
    private static UserRepository instance;
    private readonly IMongoDatabase database;
    
    UserRepository()
    {
        connectionString = Environment.GetEnvironmentVariable("MongodbConnectionURI");

        if (connectionString == null)
        {
            Console.WriteLine("Error retrieving mongodb connection uri");
        }

        var settings = MongoClientSettings.FromConnectionString(connectionString);
        settings.ServerApi = new ServerApi(ServerApiVersion.V1);
        
        var client = new MongoClient(settings);
        database = client.GetDatabase("tuturu");
    }

    public static UserRepository GetInstance()
    {
        if (instance == null)
        {
            instance = new UserRepository();
        }

        return instance;
    }

    public async Task AddCoquillette(ulong userId, string name)
    {
        var filter = Builders<UserModel>.Filter
            .Eq(user => user.UserId, userId);
        
        var collection = database.GetCollection<UserModel>("users");
        
        var user = (await collection.FindAsync(filter)).FirstOrDefault();
        
        if (user == null)
        {
            user = new UserModel
            {
                UserId = userId,
                Name = name,
                CoquillettesCount = 1
            };
            await collection.InsertOneAsync(user);
        }
        else
        {
            var update = Builders<UserModel>.Update
                .Set(userToUpdate => userToUpdate.CoquillettesCount, user.CoquillettesCount+1);
            await collection.UpdateOneAsync(filter, update);
        }
    }
}