using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Bot.Datasource;

[Serializable]
public class UserModel
{
    [BsonId, BsonElement("_id"), BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    
    [BsonElement("userId"), BsonRepresentation(BsonType.Int64)]
    public ulong UserId { get; set; }
    
    [BsonElement("name"), BsonRepresentation(BsonType.String)]
    public string Name { get; set; }
    
    [BsonElement("coquillettesCount"), BsonRepresentation(BsonType.Int64)]
    public long CoquillettesCount { get; set; }
}